
function loadFilm() {
    fetch('https://ajax.test-danit.com/api/swapi/films')
        .then((res) => res.json())
        .then((data) => {
            const div = document.createElement('div')
            data.forEach((item) => {
                const itemDiv = document.createElement('div')
                itemDiv.innerHTML = `
                <h3>${item.episodeId}. ${item.name}</h3>
                <p>${item.openingCrawl}
                `
                document.body.append(div)
                div.append(itemDiv)

                const list = document.createElement('ul')
                const char = item.characters
                let load = char.map((item) => {
                    return fetch(item)
                        .then(function (response) {
                            return response.json()
                        })
                        .then(function (name) {
                            return name.name
                        })

                })
                Promise.all(load)
                    .then((dataInfo) => {
                        dataInfo.forEach(names => {
                            const li = document.createElement('li')
                            li.innerHTML = `${names}`;
                            list.append(li);
                        })
                        itemDiv.append(list)
                    })

            })

        })
}

loadFilm()