class Employee {
    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
    }
    set name(name) {
        this._name = name
    }
    get name() {
        return this._name
    }
    set age(age) {
        this._age = age
    }
    get age() {
        return this._age
    }
    set salary(salary) {
        this._salary = salary
    }
    get salary() {
        return this._salary
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang
    }

    get salary() {
        return this._salary
    }

    set salary(salary) {
        this._salary = salary * 3
    }
}

let person1 = new Programmer("Ivan", 22, 10000, "js")
let person2 = new Programmer("Kirill", 25, 15000, "phyton")
let person3 = new Programmer("Petro", 28, 20000, "c#")

console.log(person1);
console.log(person2);
console.log(person3);
