const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];


const div = document.querySelector('#root')
const title = document.createElement('h1')
const list = document.createElement('ul');
div.append(title, list);
title.innerHTML = 'Список книг'


function uploadBookList() {

    for (const key in books) {
        const { author, name, price } = books[key];
        let listLi = document.createElement('li');
        listLi.innerHTML = `Автор: ${author}.<br> <strong>"${name}"</strong> <br> Цена: ${price}$.`;

        try {
            if (author === undefined || name === undefined || price === undefined) {
                if (!Object.keys(books[key]).includes('author')) {
                    throw new ReferenceError('Автор книги № ' + key + ' не найден')
                }
                else if (!Object.keys(books[key]).includes('name')) {
                    throw new ReferenceError('Название книги № ' + key + ' не найдена')
                }
                else {
                    throw new ReferenceError('Цена книги № ' + key + ' не найдена')
                }
            }
            list.append(listLi);
        } catch (e) {
            console.log(e);
        }
    }

}
uploadBookList()