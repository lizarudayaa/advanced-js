const wrapper = document.createElement('div')
const IPBtn = document.createElement('button')

IPBtn.innerText = 'Вычислить по IP'

wrapper.append(IPBtn)
document.body.append(wrapper)

IPBtn.addEventListener('click', getUserIP)

async function getUserIP() {
    const userInfo = document.createElement('div')
    const ip = await fetch('https://api.ipify.org/?format=json').then(ip => ip.json());
    const location = await fetch(`http://ip-api.com/json/${ip.ip}?lang=eu&fields=continent,country,regionName,city,district`).then(location => location.json());
    const template = `
    <p>User IP: ${ip.ip}</p>
    <p>User is currently located in the <strong>${location.continent}</strong> continent in <strong>${location.country}</strong> city <strong>${location.city}</strong> </p>
    <p>User region: <strong>${location.regionName}</strong> and district: <strong>${location.district}</strong></p>
    `
    userInfo.innerHTML = template;
    wrapper.append(userInfo)
}